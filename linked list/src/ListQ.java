public class ListQ {
    Link head;
    Link tail;
    int size;

    public void addToFront(String data){
        Link elem = new Link();
        elem.data = data;

        // Проверка если есть head не null, то первому и последнему присваиваем elem
        if (head == null){
            head = elem;
            tail = elem;
        }
        else{ // иначе сдвигаем и добавляем в начало новый элемент
            elem.next = head;
            //elem.previous = head;
            head = elem;
        }
        size++;

    }
    public void addToBack(String data){
        Link elem = new Link();
        elem.data = data;

        if (tail == null){
            head = elem;
            tail = elem;
        }
        else{
            tail.next = elem;
            tail = elem;
        }
        size++;
    }

    public void addByIndex(String data, String in_data){
        Link temp = head;
        Link elem = new Link();
        elem.data = data;

        while (temp != null){
            if(temp.data.equals(in_data)){
                elem.next = temp.next;
                temp.next = elem;
            }
            temp = temp.next;
        }
        size++;
    }

    public void removeFromFront(){
        if (!(isEmpty())){
            if(head == null){
                System.out.println("List is empty");
            }
            else{
                head = head.next;
            }
            size--;
        }
        else{
            System.out.println("List is empty");
        }
    }

    public void removeFromBack(){
        if (!isEmpty()){
            if(tail == null){
                System.out.println("List is empty");
            }
            else {
                Link temp = head;
                Link t = new Link();

                while (temp.next != null){
                    t = temp;
                    temp = temp.next;
                }
                tail = t;
                tail.next = null;
            }
            size--;
        }
        else{
            System.out.println("List is empty");
        }
    }

    public void findElement(String data){
        if (!(isEmpty())){
            Link temp = head;
            int ind = 1;

            while (temp != null){
                if(temp.data.equals(data)){
                    System.out.println("Position: " + ind + "; " + temp.data);
                    return;
                }

                temp = temp.next;
                ind++;
            }
            System.out.println(data + " isn't the list");
        }
        else{
            System.out.println("List is empty");
        }
    }

    private boolean isEmpty(){
        if (size == 0) return true;

        return false;
    }

    // вывод на экран
    public void printQ(){
        if (!(isEmpty())){
            //Создаем новый экземпляр класса
            Link temp = head;


            while(temp != null){
                System.out.println(temp.data);
                temp = temp.next;
            }
        }
        else{
            System.out.println("List is empty");
        }
    }
}
