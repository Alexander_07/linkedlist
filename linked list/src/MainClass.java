public class MainClass {
    public static void main(String[] args) {
        ListQ list = new ListQ();

        list.addToFront("Alex");
        list.addToFront("Sam");
        list.addToBack("Jack");
        list.addByIndex("Tim", "Sam");
        list.printQ();
    }
}
